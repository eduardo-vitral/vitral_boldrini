# vitral_boldrini

## Attribution

Please cite [us](https://ui.adsabs.harvard.edu/abs/2021arXiv211201265V/abstract) if you use the videos in this repository. The BibTeX entry for the paper is:

```bibtex
@ARTICLE{2022A&A...667A.112V,
       author = {{Vitral}, Eduardo and {Boldrini}, Pierre},
        title = "{Properties of globular clusters formed in dark matter mini-halos}",
      journal = {\aap},
     keywords = {globular clusters: general, dark matter, galaxies: kinematics and dynamics, galaxies: formation, galaxies: dwarf, methods: numerical, Astrophysics - Astrophysics of Galaxies, Astrophysics - Cosmology and Nongalactic Astrophysics},
         year = 2022,
        month = nov,
       volume = {667},
          eid = {A112},
        pages = {A112},
          doi = {10.1051/0004-6361/202244530},
archivePrefix = {arXiv},
       eprint = {2112.01265},
 primaryClass = {astro-ph.GA},
       adsurl = {https://ui.adsabs.harvard.edu/abs/2022A&A...667A.112V},
      adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}
```

